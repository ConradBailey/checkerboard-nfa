#ifndef CHECKERBOARD_H
#define CHECKERBOARD_H

#include <vector>
#include <queue>
#include <string>

class Checkerboard {
// I like this bit. It's pretty much the minimal interface for an automaton
public:
  static const char symbols[2];

  Checkerboard(std::size_t, std::size_t);
  Checkerboard(const Checkerboard&);

  bool checkValidity(const std::string&);

  Checkerboard& operator=(const Checkerboard&);
private:
  struct square {
    bool state = false;
    bool newState = false;
    char color = Checkerboard::symbols[0];
  };

  typedef std::vector<square> RowVector;
  typedef std::vector<RowVector> BoardVector;

  std::size_t rows, cols;
  BoardVector board;
  // I would like these to be references, but they're relative to the underlying vectors.
  // so I'm unsure of how to initialize them if they were references.
  bool& startState;
  bool& finalState;

  void getNeighbors(std::queue<square*>&, std::size_t, std::size_t);
  void initialize();
  void takeTurn(char);
  void updateBoard();
  void reset();
};

#endif
