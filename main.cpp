// The program is explained in full in the README

#include <iostream>
#include <queue>
#include <string>
#include "Checkerboard.h"

void queueStrings(std::queue<std::string>&, const std::string&, int);
void checkValidity(const std::string&);

//Easily change dimensions of the board constructed
const std::size_t ROWS = 3;
const std::size_t COLS = 3;

int main() {
  std::cout << "Length: ";
  std::size_t length;
  std::cin >> length;

  //queues all permutations of substrings
  std::queue<std::string> symbolQueue;
  for (std::size_t i = 0; i <= length; ++i)
    queueStrings(symbolQueue, "", i);

  Checkerboard board(ROWS, COLS);
  while (!symbolQueue.empty()) {
    if (board.checkValidity(symbolQueue.front()))
      std::cout << symbolQueue.front() << std::endl;
    symbolQueue.pop();
  }
}

// A recursive function that pushes all permutations of strings
// of symbols of the input length to the queue
// e.g. (length = 2) -> {"rr","rb","bb","br"}
void queueStrings(std::queue<std::string>& symbolQueue, const std::string& inputString, int max) {
  if (!max)
    symbolQueue.push(inputString);
  else
    for (char symbol : Checkerboard::symbols)
      queueStrings(symbolQueue, inputString + symbol, max - 1);
}


