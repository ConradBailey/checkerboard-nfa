EXECUTABLE=Checkerboard-NFA
CXX=g++
CXXFLAGS=-ggdb -g -W -Wall -Wextra -Werror -std=c++11

$(EXECUTABLE): main.cpp Checkerboard.o
	$(CXX) $(CXXFLAGS) Checkerboard.o main.cpp -o $(EXECUTABLE)

Checkerboard.o: Checkerboard.h

clean:
	rm Checkerboard.o $(EXECUTABLE)
