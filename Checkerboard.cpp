#include "Checkerboard.h"

//To add colors modify this enum and the symbols[] below.
//The first element of this array will be the default color of a square
const char Checkerboard::symbols[2] = {'b','r'};

//I'm kinda proud of this function. Gets all the adjacent elements in a double array. Handles edges nicely.
//I'm proud because a lot of people implement this with a mess of if{} statements, but I thnk this is fairly
//clean.
void Checkerboard::getNeighbors(std::queue<Checkerboard::square*>& validNeighbors, std::size_t r, std::size_t c) {
  std::size_t minR = (r == 0) ? r : r - 1;
  std::size_t minC = (c == 0) ? c : c - 1;
  std::size_t maxR = (r == rows - 1) ? r : r + 1;
  std::size_t maxC = (c == cols - 1) ? c : c + 1;

  for (std::size_t i = minR; i <= maxR; ++i)
    for (std::size_t j = minC; j <= maxC; ++j)
      if (i != r || j != c)
        validNeighbors.push(&board[i][j]);
}

//Re-define this to expand the classes colors or set-up patterns beside checkerboard
void Checkerboard::initialize() {
  for (Checkerboard::BoardVector::size_type i = 0; i < board.size(); ++i)
    for (Checkerboard::RowVector::size_type j = !(i % 2); j < board[i].size(); j += 2)
      board[i][j].color = 'r';

  startState = true;
}

Checkerboard::Checkerboard(std::size_t r, std::size_t c)
  : rows(r),
    cols(c),
    board(BoardVector(r, RowVector(c))),
    startState(board[0][0].state),
    finalState(board[rows - 1][cols - 1].state) {

  Checkerboard::initialize();
}

Checkerboard::Checkerboard(const Checkerboard& src)
  : Checkerboard::Checkerboard(src.rows, src.cols) {
}

bool Checkerboard::checkValidity(const std::string& inputString) {
  for (char symbol : inputString)
    Checkerboard::takeTurn(symbol);
  bool validity = finalState;

  Checkerboard::reset();
  return validity;
}

void Checkerboard::takeTurn(char symbol) {
  std::queue<square*> neighbors;
  // Does the actual processing of the symbols.
  for (Checkerboard::BoardVector::size_type i = 0; i != board.size(); ++i) {
    for (Checkerboard::RowVector::size_type j = 0; j != board[i].size(); ++j) {
      if (board[i][j].state) {
        Checkerboard::getNeighbors(neighbors, i, j);
        while (!neighbors.empty()) {
          if (symbol == neighbors.front()->color)
            neighbors.front()->newState = true;
          neighbors.pop();
        }
      }
    }
  }
  Checkerboard::updateBoard();
}

void Checkerboard::updateBoard() {
  // I couldn't think of a clever way to calculate the new states in-place, so here we update all the states
  for (Checkerboard::RowVector& row : board) {
    for (Checkerboard::square& tempSquare : row) {
      if (tempSquare.newState || tempSquare.state) {
        tempSquare.state = tempSquare.newState;
        tempSquare.newState = false;
      }
    }
  }
}

void Checkerboard::reset() {
  for (Checkerboard::RowVector& row : board)
    for (Checkerboard::square& tempSquare : row)
      tempSquare.state = tempSquare.newState = false;
  startState = true;
}

Checkerboard& Checkerboard::operator=(const Checkerboard& src) {
  rows = src.rows;
  cols = src.cols;

  board.resize(rows, RowVector(cols));

  startState = board[0][0].state;
  finalState = board[rows - 1][cols - 1].state;

  Checkerboard::initialize();

  return *this;
}
